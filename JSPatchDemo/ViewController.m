//
//  ViewController.m
//  JSPatchDemo
//
//  Created by 刘春雷 on 16/8/28.
//  Copyright © 2016年 Penton. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initUI];
    
    [self test];
}

- (void)initUI {

    self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 100, 200, 30)];
    self.label.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:self.label];
}

- (void)test {

    self.label.text = @"abcdefg";
}

@end

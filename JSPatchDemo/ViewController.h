//
//  ViewController.h
//  JSPatchDemo
//
//  Created by 刘春雷 on 16/8/28.
//  Copyright © 2016年 Penton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) UILabel *label;

@end

